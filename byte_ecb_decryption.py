
"""
Set 2 - Challenge 12
"""

from Crypto.Cipher import AES
from collections import Counter
from string import printable

from block_crypto import pkcs7_pad
from conversion import b64_to_hex
from detection import Detector, EncryptChoice
from encryption import AESEncrypter
from utils import generate_random_key


RAND_KEY = generate_random_key(AES.block_size)


class DetectionOracle(Detector):
    def __init__(self, cipher: bytes):
        self.cipher = cipher

    def detect_ecb_or_ebc(self):
        blocks = self.split_into_blocks(self.cipher, AES.block_size)
        c = Counter(blocks)
        most_likely_block, _ = c.most_common(1)[0]

        encrypt_choice_guess = [EncryptChoice.CBC] * len(blocks)
        ecb_indices = [index for index, b in enumerate(blocks) if b == most_likely_block]

        for index in ecb_indices:
            encrypt_choice_guess[index] = EncryptChoice.ECB

        return encrypt_choice_guess


class BlockCipherOracle:
    def __init__(self, plaintext: bytes):
        self.plaintext = plaintext

    @staticmethod
    def append_pad(text: bytes, pad_byte: bytes, pad_amount: int):
        return text + (pad_byte * pad_amount)

    @staticmethod
    def prepend_pad(text: bytes, pad_byte: bytes, pad_amount: int):
        return BlockCipherOracle.append_pad(text[::-1], pad_byte[::-1], pad_amount)[::-1]

    @staticmethod
    def attach_file_str(plaintext, file_path):
        with open(file_path) as f:
            file_text = f.read()
        return BlockCipherOracle.append_pad(plaintext, b64_to_hex(file_text), 1)

    def encrypt(self):
        random_key = RAND_KEY
        padded_plaintext = self.attach_file_str(self.plaintext, file_path='challenge12_ecb_decrypt.txt')
        plaintext_candidate_block = pkcs7_pad(padded_plaintext, AES.block_size, b'\x04')

        blocks = [plaintext_candidate_block[a:a+AES.block_size] for a in
                  range(0,len(plaintext_candidate_block),AES.block_size)]
        ciphertext_list = list()

        for block in blocks:
            # print("Encrypting in ECB mode...")
            ciphertext_list.append(AESEncrypter.ecb(block, random_key))

        return b''.join(ciphertext_list)


class ECBDefeat:
    def __init__(self):
        self.printable = set(map(lambda x: x.encode(), printable))

    @staticmethod
    def detect_block_size(max) -> int:
        search_block = BlockCipherOracle(b'').encrypt()
        search_block = search_block[:len(search_block)//2]
        for block_size in range(2, max + 1):
            new_cipher = BlockCipherOracle(b'A'*block_size).encrypt()
            if search_block in new_cipher:
                return block_size
        return -1

    @staticmethod
    def detect_if_ecb(ciphertext):
        detected = DetectionOracle(cipher=ciphertext).detect_ecb_or_ebc()
        return detected[0] is EncryptChoice.ECB

    def run(self):
        block_size = self.detect_block_size(52)
        unmodified_cipher = BlockCipherOracle(b'').encrypt()
        if self.detect_if_ecb(ciphertext=BlockCipherOracle(b'A'*32).encrypt()):
            blocks = DetectionOracle.split_into_blocks(unmodified_cipher, block_size)
            solution_so_far = b''

            for block_index, block in enumerate(blocks):
                block_solved_so_far = b''

                for pad_size in range(block_size - 1, -1, -1):
                    pad = b'A' * pad_size
                    block_to_match = BlockCipherOracle(pad)\
                                         .encrypt()[block_size*block_index:block_size*(block_index + 1)]

                    for guessed_char in self.printable:
                        match_candidate_block = BlockCipherOracle(pad + solution_so_far + block_solved_so_far +
                                                                  guessed_char).encrypt()
                        match_candidate_block = match_candidate_block[block_index*block_size:block_size*(block_index + 1)]
                        if block_to_match == match_candidate_block:
                            block_solved_so_far = block_solved_so_far + guessed_char
                            break
                solution_so_far = solution_so_far + block_solved_so_far
            print(solution_so_far)
        else:
            print("ECB mode not detected")


if __name__ == '__main__':
    ECBDefeat().run()
