
from Crypto.Cipher import AES
from Crypto.Util import strxor
from collections import namedtuple
from enum import Enum
from random import randint
from secrets import randbelow

from block_crypto import pkcs7_pad
from utils import generate_random_key


class EncryptChoice(Enum):
    ECB = 0
    CBC = 1


class AESEncrypter:
    @staticmethod
    def ecb(plaintext: bytes, key: bytes):
        obj = AES.new(key, AES.MODE_ECB)
        return obj.encrypt(plaintext)


# Set 2 - Challenge 10
class CBC:
    def __init__(self, key: bytes, IV: bytes, pad_amount: int, pad_byte: bytes):
        self.IV = pkcs7_pad(IV, pad_amount, pad_byte)
        self.key = key

    def block_xor(self, key1: bytes, key2: bytes):
        assert len(key1) == len(key2)
        return strxor.strxor(key1, key2)

    def encrypt(self, block_size: int, plaintext: bytes):
        plaintext_candidate_block = pkcs7_pad(plaintext, block_size, b'\x04')
        blocks = [plaintext_candidate_block[a:a+block_size] for a in range(0,len(plaintext_candidate_block),block_size)]
        xor_pairs = len(blocks) * [self.IV]
        encrypted_blocks = list()

        BlockPair = namedtuple('BlockPair', ['block', 'xor_pair'])
        for i, block_pair in enumerate(zip(blocks, xor_pairs)):
            bp = BlockPair(block=block_pair[0],  xor_pair=block_pair[1])
            xor_result = self.block_xor(bp.block, bp.xor_pair)
            block_encrypt = AESEncrypter.ecb(xor_result, key=self.key)
            encrypted_blocks.append(block_encrypt)
            try:
                xor_pairs[i+1] = block_encrypt
            except IndexError:
                break

        return b''.join(encrypted_blocks)

    def decrypt(self, block_size: int, ciphertext: bytes):
        assert len(ciphertext) % block_size == 0
        blocks = [ciphertext[a:a+block_size] for a in range(0,len(ciphertext),block_size)]
        xor_pairs = blocks.copy()
        xor_pairs.pop()
        xor_pairs.insert(0, self.IV)

        decrypted_blocks = list()

        BlockPair = namedtuple('BlockPair', ['block', 'xor_pair'])
        for i, block_pair in enumerate(zip(blocks, xor_pairs)):
            bp = BlockPair(block=block_pair[0],  xor_pair=block_pair[1])
            block_decrypt = AESDecrypter.ecb(bp.block, key=self.key)
            xor_result = self.block_xor(block_decrypt, bp.xor_pair)
            decrypted_blocks.append(xor_result)

        return b''.join(decrypted_blocks)


# Set 2 - Challenge 11
class RandomEncrypt:
    def __init__(self, plaintext: bytes):
        self.plaintext = plaintext

    @staticmethod
    def append_pad(text: bytes, pad_byte: bytes, pad_amount: int):
        return text + (pad_byte * pad_amount)

    @staticmethod
    def prepend_pad(text: bytes, pad_byte: bytes, pad_amount: int):
        return RandomEncrypt.append_pad(text[::-1], pad_byte, pad_amount)[::-1]

    def encrypt(self):
        random_key = generate_random_key(AES.block_size)
        padded_plaintext = self.prepend_pad(self.plaintext, b'\x04', randint(5, 10))
        padded_plaintext = self.append_pad(padded_plaintext, b'\x04', randint(5, 10))
        plaintext_candidate_block = pkcs7_pad(padded_plaintext, AES.block_size, b'\x04')

        blocks = [plaintext_candidate_block[a:a+AES.block_size] for a in
                  range(0,len(plaintext_candidate_block),AES.block_size)]
        ciphertext_list = list()
        encryption_choice = list()

        for block in blocks:
            if EncryptChoice(randbelow(2)) is EncryptChoice.ECB:
                print("Encrypting in ECB mode...")
                ciphertext_list.append(AESEncrypter.ecb(block, random_key))
                encryption_choice.append(EncryptChoice.ECB)
            else:
                print("Encrypting in CBC mode...")
                cbc = CBC(random_key, generate_random_key(AES.block_size), AES.block_size, b'\x04')
                ciphertext_list.append(cbc.encrypt(AES.block_size, block))
                encryption_choice.append(EncryptChoice.CBC)

        return b''.join(ciphertext_list), encryption_choice


if __name__ == '__main__':
    from decryption import AESDecrypter
    from conversion import b64_to_hex

    cbc = CBC(b"YELLOW SUBMARINE", b"0", pad_amount=AES.block_size, pad_byte=b"\x04")
    cipher = cbc.encrypt(block_size=AES.block_size, plaintext=b"hello there is no spoon. there is no cake. there can be no walk.")
    print(cipher)

    print(cbc.decrypt(block_size=AES.block_size, ciphertext=cipher))

    with open("cbc_encrypted_file.txt") as f:
        file_cipher = b64_to_hex(f.read())

    cbc = CBC(b"YELLOW SUBMARINE", b"\x00", pad_amount=AES.block_size, pad_byte=b"\x00")
    print(cbc.decrypt(block_size=16, ciphertext=file_cipher))

    plaintext = b"I'm back and I'm"
    key = b"YELLOW SUBMARINE"
    cipher = AESEncrypter.ecb(plaintext=plaintext, key=key)

    print(cipher)
    print(AESDecrypter.ecb(cipher, key))

    re = RandomEncrypt(b"I'm back and hello there is no spoon. there is no cake. there can be no walk.g kick in \n\
    It controls my mouth and I begin \nTo just let it flow, let my concepts go \nMy posse's to the side yellin', Go \
    Vanilla Go! \n\nSmooth 'cause that's the way I will be \nAnd if you don't give a damn, then \nWhy you starin' at \
    me \nSo get off 'cause I control the stage \nThere's no dissin' allowed \nI'm in my own phase \nThe girlies say \
    they love me and that is ok \nAnd I can dance better than any kid n' play \n\nStage 2 -- Yea the one ya' wanna \
    listen to \nIt's off my head so let the beat play through \nSo I can funk it up and make it sound good \n1-2-3 \
    Yo -- Knock on some wood \nFor good luck, I like my rhymes atrocious \nSupercalafragilisticexpialidocious \nI'm ")

    cipher, _ = re.encrypt()
    print(cipher)
