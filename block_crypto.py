

# Set 2 - Challenge 9
def pkcs7_pad(block: bytes, pad_block_size: int, pad_char: bytes):
    if (len(block) < pad_block_size):
        return block + (pad_block_size - len(block)) * pad_char
    elif (len(block) % pad_block_size == 0):
        return block

    pad_total_size = ((len(block) // pad_block_size) + 1 ) * pad_block_size
    return block + (pad_total_size - len(block)) * pad_char


if __name__ == '__main__':
    print(pkcs7_pad(b"YELLOW SUBMARINE", 20, b"\x04"))
