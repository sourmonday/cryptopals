
from random import randint


# Set 2 - Challenge 11
def generate_random_key(length: int) -> bytes:
    key_list = list()
    for _ in range(length):
        key_list.append(randint(0, 255))
    return bytes(key_list)