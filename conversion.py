
from base64 import b64encode, b64decode


# Set 1 - Challenge 1
def hex_to_b64(input: bytes):
    return b64encode(input)

def b64_to_hex(input: str):
    return b64decode(input)


if __name__ == '__main__':
    import binascii

    input_str = '49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d'
    print(hex_to_b64(binascii.unhexlify(input_str)))
