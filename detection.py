
import binascii
import itertools

from abc import abstractmethod
from collections import Counter
from Crypto.Cipher import AES
from decryption import BruteForcer
from encryption import EncryptChoice, RandomEncrypt


class Detector:
    @staticmethod
    def split_into_blocks(cipher: bytes, block_size: int):
        return [cipher[i:i+block_size] for i in range(0, len(cipher), block_size)]

    def parse_file(self, file_location):
        with open(file_location) as f:
            self.ciphers = f.readlines()
        self.ciphers = list(map(str.strip, self.ciphers))
        self.ciphers = list(map(binascii.unhexlify, self.ciphers))

    @abstractmethod
    def solve_ciphers(self):
        pass


# Set 1 - Challenge 4
class SingleByteXORDetection(Detector):
    # Highest scoring cipher is returned. Manually inspect if it is English
    def solve_ciphers(self):
        solvers = list(map(BruteForcer, self.ciphers))
        solutions = list(map(lambda solver: solver.solve_single_char(), solvers))
        solutions = list(map(lambda x: x[0], solutions))
        # solutions = list(map(binascii.unhexlify, solutions))

        solution_scores = list(map(BruteForcer.score_solution, solutions))

        scored_solution = list(zip(solution_scores, solutions))
        scored_solution.sort(reverse=True)
        return scored_solution[0][1]


# Set 1 - Challenge 8
class ECBDetection(Detector):
    def calculate_duplicates(self):
        blocks = list(map(self.split_into_blocks, self.ciphers, itertools.repeat(16, len(self.ciphers))))
        unique_blocks = list(map(set, blocks))

        len_blocks = list(map(len, blocks))
        len_uniques = list(map(len, unique_blocks))

        differences = [a_i - b_i for a_i, b_i in zip(len_blocks, len_uniques)]
        return list(zip(differences, self.ciphers))

    def solve_ciphers(self):
        list_of_differences = self.calculate_duplicates()
        list_of_differences.sort(reverse=True)
        return list_of_differences[0]


# Set 2 - Challenge 11
class DetectionOracle(Detector):
    def __init__(self, plaintext: bytes):
        re = RandomEncrypt(plaintext=plaintext)
        self.cipher, self.answer_key = re.encrypt()

    def detect_ecb_or_ebc(self):
        blocks = self.split_into_blocks(self.cipher, AES.block_size)
        c = Counter(blocks)
        most_likely_block, _ = c.most_common(1)[0]

        encrypt_choice_guess = [EncryptChoice.CBC] * len(blocks)
        ecb_indices = [index for index, b in enumerate(blocks) if b == most_likely_block]

        for index in ecb_indices:
            encrypt_choice_guess[index] = EncryptChoice.ECB

        return encrypt_choice_guess

    def check_accuracy(self, guesses: list) -> float:
        correct = 0
        for idx, guess in enumerate(guesses):
            if guess == self.answer_key[idx]:
                correct += 1

        return (correct / len(guesses)) * 100

    def solve_ciphers(self):
        raise NotImplementedError


if __name__ == '__main__':
    detector = SingleByteXORDetection()
    detector.parse_file('single_byte_xor_search.txt')
    print(detector.solve_ciphers())

    detector = ECBDetection()
    detector.parse_file("aes_ecb_search_challenge.txt")

    detector.calculate_duplicates()
    print(detector.solve_ciphers())

    oracle = DetectionOracle(b"AAAAA" * 3000)
    guesses = oracle.detect_ecb_or_ebc()
    print("Oracle accuracy: %f%%\n" % oracle.check_accuracy(guesses=guesses))
