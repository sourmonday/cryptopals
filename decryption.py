
import binascii
import sys

from crypto_scores import SCORES
from Crypto.Cipher import AES
from collections import namedtuple
from string import printable
from xorlib import *


# Set 1 - Challenge 3
class BruteForcer:
    def __init__(self, cipher: bytes):
        self.cipher = cipher

    @staticmethod
    def score_solution(solution: bytes) -> float:
        score = 0
        for char in solution:
            if chr(char) not in printable:
                score -= 1
            elif chr(char) not in SCORES.keys():
                score -= 0.01
            else:
                score += SCORES[chr(char).lower()]
        return score

    def solve_single_char(self):
        max_score = -sys.maxsize - 1
        current_result = None
        for key in range(256):
            result = single_byte_xor(self.cipher, chr(key))
            result_score = self.score_solution(result)

            if max_score < result_score:
                current_result = (result, key)
                max_score = result_score

        return current_result

    @staticmethod
    def calculate_hamming_distance(key1, key2):
        assert len(key1) == len(key2)
        xored = xor(key1, key2)

        distance = 0
        for char in xored:
            distance += sum(xb == '1' for xb in bin(char))
        return distance

    def calculate_normalized_hamming_dist_for_keysizes_up_to(self, keysize_max: int, return_amount: int) -> list:
        EditDistances = namedtuple('EditDistances', ['keysize', 'norm_distance'])
        normalized_distances = list()
        for keysize in range(2, keysize_max + 1):
            key1 = self.cipher[:keysize]
            key2 = self.cipher[keysize:2*keysize]
            normalized_distances.append(EditDistances(keysize, self.calculate_hamming_distance(key1, key2)/keysize))

        normalized_distances.sort(key = lambda x: x[1])
        return normalized_distances[:return_amount]

    def transpose_cipher(self, block_count: int) -> list:
        transposed_blocks = list()
        for offset in range(block_count):
            transposed_blocks.append(self.cipher[offset::block_count])
        return transposed_blocks

    def solve_repeating_key(self):
        top_keysize_guesses = self.calculate_normalized_hamming_dist_for_keysizes_up_to(40, 3)
        guesses = list()

        for keysize_guess, _ in top_keysize_guesses:
            transposed_sets = self.transpose_cipher(keysize_guess)
            solvers = list(map(BruteForcer, transposed_sets))
            solutions = list(map(lambda solver: solver.solve_single_char(), solvers))
            key = [y for _, y in solutions]
            key = list(map(chr, key))
            guesses.append(''.join(key))
            guesses[-1] = guesses[-1]

        for key in guesses:
            print(repeating_key_xor(self.cipher, key))


class AESDecrypter:
    @staticmethod
    def ecb(ciphertext: bytes, key: bytes):
        obj = AES.new(key, AES.MODE_ECB)
        return obj.decrypt(ciphertext)


if __name__ == '__main__':
    from conversion import b64_to_hex
    # Set 1 - Challenge 6
    with open('base64_set1_6_challenge.txt') as f:
        challenge_cipher = b64_to_hex(f.read())

    bf = BruteForcer(challenge_cipher)
    bf.solve_repeating_key()
    print(BruteForcer.calculate_hamming_distance(b"this is a test", b"wokka wokka!!!"))

    # Set 1 - Challenge 3
    s = '1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736'
    bf = BruteForcer(binascii.unhexlify(s))

    print(bf.solve_single_char())

    # Set 1 - Challenge 7
    key = b"YELLOW SUBMARINE"

    with open("aes_ecb_yellowsubmarine.txt") as f:
        challenge_cipher = b64_to_hex(f.read())

    decrypter = AESDecrypter()
    print(decrypter.ecb(challenge_cipher, key))
