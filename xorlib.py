
from binascii import hexlify


# Set 1 - Challenge 2
def xor(a: bytes, b: bytes):
    xored = [chr(x ^ y).encode() for x, y in zip(a, b)]
    return b''.join(xored)


def single_byte_xor(cipher: bytes, key: str):
    assert len(key) == 1
    repeating_key_str = key * len(cipher)
    return xor(cipher, repeating_key_str.encode())


# Set 1 - Challenge 5
def repeating_key_xor(cipher: bytes, key: str):
    key_to_use = key.encode()
    if len(key) < len(cipher):
        key_to_use = key * ((len(cipher) // len(key)) + 1)
        key_to_use = key_to_use.encode()
    key_to_use = key_to_use[:len(cipher)]
    return xor(cipher, key_to_use)


if __name__ == '__main__':
    from binascii import hexlify, unhexlify

    string1 = '1c0111001f010100061a024b53535009181c'
    string2 = '686974207468652062756c6c277320657965'

    print(xor(unhexlify(string1), unhexlify(string2)))

    # Set 1 - Challenge 5
    message = "Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal"
    print(hexlify(repeating_key_xor(message.encode(), "ICE")))
